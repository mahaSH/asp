﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace day03blog
{
    public partial class signin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btLogin_Click(object sender, EventArgs e)
        {
            //CONNECT TO DATABASE
            string connetionString;
            SqlDataReader dataReader;
            SqlConnection cnn;
            connetionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\AQ-37\Documents\asp\day03blog\App_Data\day02blog.mdf;Integrated Security=True";
            cnn = new SqlConnection(connetionString);
            cnn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand();

            //GET THE INPUTS 
            string name = Request["tbName"];
            string password = Request["tbPassword"];
            string dbPassword = "";
            //1-NAME SHOULD NOT EXIST IN DATABASE
            cmd = new SqlCommand("SELECT password FROM users WHERE name = @user", cnn);
            cmd.Parameters.AddWithValue("@user", name);
            dataReader = cmd.ExecuteReader();
            while (dataReader.Read())
            {
                dbPassword = dataReader.GetString(0);

                if (dbPassword != password)
                {
                    Response.Write("<br />-Please enter correct data");

                }
                else
                {
                    Session["Name"] = name;
                    Response.Redirect("login_success.aspx");
                    
                }
            }
        }
    }
}