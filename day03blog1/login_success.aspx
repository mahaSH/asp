﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login_success.aspx.cs" Inherits="day03blog.login_success" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">

        Successfully logged in <br />
&nbsp;&nbsp;
        <asp:Button ID="btContinue" runat="server" Text="continue" Width="54px" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btLogout" runat="server" OnClick="btLogout_Click" Text="Logout" Width="54px" />
    </form>
</body>
</html>
