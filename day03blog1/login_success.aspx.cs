﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace day03blog
{
    public partial class login_success : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           // Response.Write(User.Identity.Name);
        }

        protected void btLogout_Click(object sender, EventArgs e)
        {
            Session.Clear();
            Response.Redirect("logout_success.aspx");
        }
    }
}