﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Day03blog
{
    public partial class article1 : System.Web.UI.Page
    {
        int articleId = 0;
        int authorId = 0;
        SqlConnection cnn = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\AQ-37\Documents\asp\day03blog\App_Data\day02blog.mdf;Integrated Security=True;MultipleActiveResultSets=true");
        protected void Page_Load(object sender, EventArgs e)
        {
           lblWelcome.Text = "You are login as " + Session["Name"] + ".<a href=\"logout.aspx\">Logout</a>";
            articleId= int.Parse(Request.QueryString["id"]);
            cnn.Open();
           
            DateTime creationTime;
            string title = "";
            string body = "";
            int authorName ;


            SqlDataAdapter adapter = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dataReader,commentsDataReader,authorDataReader, commentAuthorDataReader;
            cmd = new SqlCommand("SELECT * FROM articles WHERE Id = @id", cnn);
            cmd.Parameters.AddWithValue("@Id", articleId);
            dataReader = cmd.ExecuteReader();
            while (dataReader.Read())
            {
                authorId = dataReader.GetInt32(1);
              //  creationTime = dataReader.GetDateTime(2);
                title = dataReader.GetString(3);
                body= dataReader.GetString(4);
                tbBody.Text = body;
                lblTitle.Text = title;
              // dataReader.Close();
                //GET AUTHOR NAME
                cmd = new SqlCommand("SELECT name FROM users WHERE id = @id", cnn);
                cmd.Parameters.AddWithValue("@id", authorId);
               authorDataReader = cmd.ExecuteReader();
                while (authorDataReader.Read())
                {
                    authorName = dataReader.GetInt32(1);
                }
                authorDataReader.Close();


                // dataReader.Close();
                //GET ARTICLES COMMENTS lblComments
                string comment = "";
                string author = "";
                cmd = new SqlCommand("SELECT * FROM comments WHERE articleId = @id", cnn);
                cmd.Parameters.AddWithValue("@id", articleId);
                commentsDataReader = cmd.ExecuteReader();
                while (commentsDataReader.Read())
                {
                    cmd = new SqlCommand("SELECT name FROM users WHERE Id = @id", cnn);
                    cmd.Parameters.AddWithValue("@id", commentsDataReader.GetInt32(1));
                    commentAuthorDataReader = cmd.ExecuteReader();
                    while (commentAuthorDataReader.Read())
                    {
                        author = commentAuthorDataReader.GetString(0);
                    }
                        comment =author+ " on " + commentsDataReader.GetDateTime(3) + ":<br/>" + commentsDataReader.GetString(4) + ".<br/>";
                    lblComments.Text += comment;
                }
                commentsDataReader.Close();


            }
            dataReader.Close();
        }

        protected void btComment_Click(object sender, EventArgs e)
        {
            //CONNECT TO DATABASE
            string connetionString;
            SqlConnection cnn;
            connetionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\AQ-37\Documents\asp\day03blog\App_Data\day02blog.mdf;Integrated Security=True";
            cnn = new SqlConnection(connetionString);
            cnn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand();
            //INSERT TO DATABASE
            cmd.CommandText = "INSERT INTO comments(articleId,authorId,creationTime,body) values (@articleId,@authorId,@creationTime,@body)";
            cmd.Parameters.AddWithValue("@articleId", articleId);
            cmd.Parameters.AddWithValue("@authorId", authorId);
            cmd.Parameters.AddWithValue("@creationTime", DateTime.Now.ToString("yyyy-MM-dd"));
            cmd.Parameters.AddWithValue("@body", tbComment.Text);
            cmd.Connection = cnn;
            cmd.ExecuteNonQuery();
            cnn.Close();
            Response.Redirect("article.aspx?id="+ articleId);
        }
    }
}