﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Day03blog
{
    public partial class article : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["Name"] == null)
            {
                Response.Redirect("login2.aspx");
            }
            else
            {
                lblWelcome.Text = "You are login as " + Session["Name"] + ".<a href=\"logout.aspx\">Logout</a>";
            }
        }

        protected void btCreate_Click(object sender, EventArgs e)
        {
            //CONNECT TO DATABASE
            string connetionString;
            SqlConnection cnn;
            connetionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\AQ-37\Documents\asp\day03blog\App_Data\day02blog.mdf;Integrated Security=True";
            cnn = new SqlConnection(connetionString);
            cnn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand();
            SqlDataReader dataReader;
            //GET THE INPUTS 
            Boolean error = false;
            int authorId=0;
            string title = Request["tbTitle"];
            string contents = Request["tbContent"];
            //GET THE AUTHORID
            cmd = new SqlCommand("SELECT id FROM users WHERE name = @user", cnn);
            cmd.Parameters.AddWithValue("@user", Session["Name"]);
            dataReader = cmd.ExecuteReader();
            while (dataReader.Read())
            {
                authorId = dataReader.GetInt32(0);
            }
            dataReader.Close();
            // CHECK THE INPUTS
            //1 - TITLE SHOULD BE 10 CHARS LONG AT LEAST
            if (title.Length<10|| title.Length > 100)
            {
                Response.Write("<br />-Title should built of 10-100 cahracters ");
                error = true;
            }
            //2-CONTENT VALIDATION
            if (contents.Length<50||contents.Length>5000)
            {
                Response.Write("<br />-Content should be 50-5000 char length.");
                error = true;
            }
            if (error == false)
            {

                cmd.CommandText = "INSERT INTO articles(authorId,title,body) values (@authorId,@title,@content)";
                cmd.Parameters.AddWithValue("@authorId", authorId);
                cmd.Parameters.AddWithValue("@title", title);
                cmd.Parameters.AddWithValue("@content", contents);
                cmd.Connection = cnn;
                cmd.ExecuteNonQuery();
                cnn.Close();
                Response.Redirect("article.aspx");
            }
        }
    }
}