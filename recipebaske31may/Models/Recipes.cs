﻿using System;
using System.Collections.Generic;

namespace recipeBasket.Models
{
    public partial class Recipes
    {
        public Recipes()
        {
            Comments = new HashSet<Comments>();
        }

        public int Id { get; set; }
        public int UserId { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public DateTime DateTime { get; set; }
        public string NumberOfServings { get; set; }
        public string Tools { get; set; }
        public string Ingredients { get; set; }
        public decimal PreparingTime { get; set; }
        public string Directions { get; set; }
        public string NutritionFacts { get; set; }
        public decimal Rate { get; set; }
        public byte[] Photo { get; set; }

        public virtual Categories Category { get; set; }
        public virtual Users User { get; set; }
        public virtual ICollection<Comments> Comments { get; set; }
    }
}
