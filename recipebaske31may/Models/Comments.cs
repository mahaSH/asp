﻿using System;
using System.Collections.Generic;

namespace recipeBasket.Models
{
    public partial class Comments
    {
        public int Id { get; set; }
        public int RecipeId { get; set; }
        public int WriterId { get; set; }
        public DateTime DateTime { get; set; }
        public string Body { get; set; }

        public virtual Recipes Recipe { get; set; }
        public virtual Users Writer { get; set; }
    }
}
