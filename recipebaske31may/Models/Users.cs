﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace recipeBasket.Models
{
    public partial class Users
    {
        public Users()
        {
            Comments = new HashSet<Comments>();
            Recipes = new HashSet<Recipes>();
        }

        public int Id { get; set; }
        [Display(Name = "Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Name is required")]
        public string Name { get; set; }
        [Display(Name = "Usernmae")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "username is required")]
        public string Username { get; set; }
        [Required(AllowEmptyStrings = false, ErrorMessage = "Password is required")]
        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "Minimum 6 characters required")]
        public string Password { get; set; }
        [NotMapped]
        [Display(Name = "ConfirmPassword")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Password confirmation is required")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "'{1}' and '{0}' do not match")]
        public string ConfirmPassword { get; set; }
        [Display(Name = "Email ID")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Email is required")]
        public string Email { get; set; }
        public string ShoppingList { get; set; }
        public byte[] Photo { get; set; }

        public virtual ICollection<Comments> Comments { get; set; }
        public virtual ICollection<Recipes> Recipes { get; set; }
    }
}
