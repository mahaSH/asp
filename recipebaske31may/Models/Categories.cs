﻿using System;
using System.Collections.Generic;

namespace recipeBasket.Models
{
    public partial class Categories
    {
        public Categories()
        {
            Recipes = new HashSet<Recipes>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string MainCategoryName { get; set; }
        public byte[] Photo { get; set; }

        public virtual ICollection<Recipes> Recipes { get; set; }
    }
}
