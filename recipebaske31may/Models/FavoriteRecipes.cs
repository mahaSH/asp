﻿using System;
using System.Collections.Generic;

namespace recipeBasket.Models
{
    public partial class FavoriteRecipes
    {
        public int RecipeId { get; set; }
        public int UserId { get; set; }
        public string Description { get; set; }
    }
}
