﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Day03blog
{
    public partial class register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //CONNECT TO DATABASE
            string connetionString;
            SqlConnection cnn;
            connetionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\AQ-37\Documents\asp\day03blog\App_Data\day02blog.mdf;Integrated Security=True";
            cnn = new SqlConnection(connetionString);
            cnn.Open();
            SqlDataAdapter adapter = new SqlDataAdapter();
            SqlCommand cmd = new SqlCommand();

            //GET THE INPUTS 
            Boolean error = false;
            string name = Request["tbName"];
            string email = Request["tbEmail"];
            string pass1 = Request["tbPass1"];
            string pass2 = Request["tbPass2"];
            // CHECK THE INPUTS
            //1-NAME SHOULD NOT EXIST IN DATABASE
            cmd = new SqlCommand("SELECT COUNT(*) FROM users WHERE name = @user", cnn);
            cmd.Parameters.AddWithValue("@user", name);
            int UserExist = (int)cmd.ExecuteScalar();
            if (UserExist > 0)
            {
                Response.Write("<br />-Name already exist,Please choose another one");
                error = true;
            }
            //2 - NAME SHOULD BUILT OF 1 - 20 SMALL LETTERS AND NUMBERS ONLY
            string pattern = @"[^a-z0-9]+";
            Match mathcedName = Regex.Match(name, pattern);
            if (mathcedName.Success || name.Length > 20 || name.Length < 1)
            {
                Response.Write("<br />-Name should built of 20  small letters and numbers only ");
                error = true;
            }
            //3-PASSWORD VALIDATION
            Match mathcedpass1 = Regex.Match(pass1, "[A-Z]");
            Match mathcedPass2 = Regex.Match(pass1, "[a-z]");
            Match mathcedPass3 = Regex.Match(pass1, "[0-9]");
            if (pass1.Length < 6 || !mathcedpass1.Success || !mathcedPass2.Success || !mathcedPass3.Success || pass1.Length > 100)
            {
                Response.Write("<br />-Password must be at least 6 characters long and must contain at least one uppercase letter, one lower case letter, and one number or special character. It must not be longer than 100 characters.");
                error = true;
            }
            //4-PASSWORD MATCHING
            if (pass1 != pass2)
            {
                Response.Write("<br />-Passwords are mismatched");
                error = true;
            }
            //5-EMAIL VALIDATION 
            string emailPattern = @"([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})";
            Match mathcedEmail = Regex.Match(email, emailPattern);
            if (email.Length == 0 || !mathcedEmail.Success)
            {
                Response.Write("<br />-Please Enter A Valid Email");
                error = true;
            }
            if (error == false)
            {

                cmd.CommandText = "INSERT INTO users(name,email,password) values (@name,@email,@pass1)";
                cmd.Parameters.AddWithValue("@name", name);
                cmd.Parameters.AddWithValue("@email", email);
                cmd.Parameters.AddWithValue("@pass1", pass1);
                cmd.Connection = cnn;
                cmd.ExecuteNonQuery();
                cnn.Close();
                Response.Redirect("register_success.aspx");
            }
        }
    }
}