﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Demo.aspx.cs" Inherits="DemoApp.Demo"  %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <TWebControl:WebControl ID="Header" runat="server" />
     <div>

	
           <asp:Label ID="lblName" runat="server" Text="Name:"></asp:Label>

	       <asp:TextBox ID="txtName" runat="server"></asp:TextBox>

	</div>
        <asp:ListBox ID="lstLocation" runat="server">
            <asp:ListItem>Mumbai</asp:ListItem>
            <asp:ListItem>Mangalore</asp:ListItem>
            <asp:ListItem>Hyderabad</asp:ListItem>
        </asp:ListBox>
        <p>
            <asp:RadioButton ID="rdMale" runat="server" OnCheckedChanged="RadioButton1_CheckedChanged" Text="Male" />
        </p>
        <asp:RadioButton ID="rdFemale" runat="server" Text="Female" />
        <p>
            <asp:CheckBox ID="chkC" runat="server" Text="C#" />
        </p>
        <asp:CheckBox ID="chkASP" runat="server" Text="ASP.NET" />
            <p>
         <asp:Button ID="btnSubmit" runat="server" OnClick="Button1_Click" Text="Submit" />
        </p>
        
    </form>
</body>
</html>
